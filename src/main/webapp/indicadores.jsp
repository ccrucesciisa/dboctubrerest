<%-- 
    Document   : index
    Created on : 08-abr-2020, 20:04:51
    Author     : javi3
--%>


<%@page import="cl.dto.ValorIndicador"%>
<%@page import="cl.dto.IndicadorDto"%>
<%@page import="cl.entities.Selecciones"%>
<%@page import="java.util.Iterator"%>

<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
  ValorIndicador valor=(ValorIndicador)request.getAttribute("valor");
 
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Estos son los valores de la UF</title>
    </head>
    <body class="text-center" >
        <form name="form" action="MantencionJugador" method="POST">  
     <div class="form-group">
                        <label for="codigo">Fecha de UF</label>
                        <input  name="codigo" value="<%= valor.getFecha()%>" class="form-control" required id="codigo" aria-describedby="usernameHelp">
                        </div>
            
               <div class="form-group">
                        <label for="codigo">Valor de UF</label>
                        <input  name="codigo" value="<%= valor.getValor()%>" class="form-control" required id="codigo" aria-describedby="usernameHelp">
                        </div>
           
        </form>
    </body>
</html>