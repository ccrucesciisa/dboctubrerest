/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.dto;

 
public class ValorIndicador {
  private String fecha;
  private float valor;


 // Getter Methods 

  public String getFecha() {
    return fecha;
  }

  public float getValor() {
    return valor;
  }

 // Setter Methods 

  public void setFecha( String fecha ) {
    this.fecha = fecha;
  }

  public void setValor( float valor ) {
    this.valor = valor;
  }
}