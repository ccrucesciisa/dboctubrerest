package cl.dto;

import java.util.ArrayList;
import java.util.List;



public class IndicadorDto {
  private String version;
  private String autor;
  private String codigo;
  private String nombre;
  private String unidad_medida;
  private List<ValorIndicador> serie =  new ArrayList<ValorIndicador>();


    /**
     * @return the serie
     */
    public List<ValorIndicador> getSerie() {
        return serie;
    }

    /**
     * @param serie the serie to set
     */
    public void setSerie(List<ValorIndicador> serie) {
        this.serie = serie;
    }

 // Getter Methods 

  public String getVersion() {
    return version;
  }

  public String getAutor() {
    return autor;
  }

  public String getCodigo() {
    return codigo;
  }

  public String getNombre() {
    return nombre;
  }

  public String getUnidad_medida() {
    return unidad_medida;
  }

 // Setter Methods 

  public void setVersion( String version ) {
    this.version = version;
  }

  public void setAutor( String autor ) {
    this.autor = autor;
  }

  public void setCodigo( String codigo ) {
    this.codigo = codigo;
  }

  public void setNombre( String nombre ) {
    this.nombre = nombre;
  }

  public void setUnidad_medida( String unidad_medida ) {
    this.unidad_medida = unidad_medida;
  }
}